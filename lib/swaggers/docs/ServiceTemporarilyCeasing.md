# AccountManagementService::ServiceTemporarilyCeasing

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**start_date** | **String** | Start date of closure | [optional] 
**end_date** | **String** | Expected end date of closure | [optional] 
**action** | **String** | Action for Temporary Ceasing | [optional] 
**reason** | **String** | Temporarily ceasing reason code | [optional] 
**reason_text** | **String** | Temporarily ceasing reason Text | [optional] 


