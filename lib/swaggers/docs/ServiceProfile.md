# AccountManagementService::ServiceProfile

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**service_id** | **String** | Unique identifier of the Service in the system | [optional] 
**start_date** | **String** | Start date for which to retrieve Service details | [optional] 
**end_date** | **String** | End date for which to retrieve Service details | [optional] 
**service_name** | **String** | Name of the Service | [optional] 
**service_name_start_date** | **String** |  | [optional] 
**service_name_end_date** | **String** |  | [optional] 
**service_type** | **String** | Primary Service Type for this CCS Service | [optional] 
**commencement_date** | **String** | Commencement Date | [optional] 
**acecqa_registration_code** | **String** | ACECQA Service Approval Number | [optional] 
**acecq_aexemptionreason** | **String** | Reason for the Service not obtaining an ACECQA Approval Numb | [optional] 
**number_of_weeks_per_year** | **Integer** | Numb. of wks/yr the srvc be opn for providing Child care | [optional] 
**number_of_weeks_per_year_date_of_event** | **String** | Date of Event for the update | [optional] 
**child_care_places** | [**RelatedCollectionChildCarePlaces**](RelatedCollectionChildCarePlaces.md) |  | [optional] 
**service_name** | [**RelatedCollectionAccountManagementServiceName**](RelatedCollectionAccountManagementServiceName.md) |  | [optional] 
**ccs_approval** | [**RelatedCollectionCCSApproval**](RelatedCollectionCCSApproval.md) |  | [optional] 
**address** | [**RelatedCollectionAddress**](RelatedCollectionAddress.md) |  | [optional] 
**contact** | [**RelatedCollectionContact**](RelatedCollectionContact.md) |  | [optional] 
**financial** | [**Financial**](Financial.md) |  | [optional] 
**fees** | [**RelatedCollectionFee**](RelatedCollectionFee.md) |  | [optional] 
**trustee** | [**RelatedCollectionTrustee**](RelatedCollectionTrustee.md) |  | [optional] 
**external_management** | [**RelatedCollectionExternalManagement**](RelatedCollectionExternalManagement.md) |  | [optional] 
**service_location_of_record** | [**RelatedCollectionServiceLocationOfRecord**](RelatedCollectionServiceLocationOfRecord.md) |  | [optional] 
**service_stop_operating** | [**ServiceStopOperating**](ServiceStopOperating.md) |  | [optional] 
**service_temporarily_ceasing** | [**RelatedCollectionServiceTemporarilyCeasing**](RelatedCollectionServiceTemporarilyCeasing.md) |  | [optional] 
**notifications** | [**Notifications**](Notifications.md) |  | [optional] 
**serious_incident** | [**SeriousIncident**](SeriousIncident.md) |  | [optional] 
**supporting_documents** | [**RelatedCollectionSupportingDocument**](RelatedCollectionSupportingDocument.md) |  | [optional] 
**approval_conditions** | [**RelatedCollectionApprovalCondition**](RelatedCollectionApprovalCondition.md) |  | [optional] 
**accs_cap_percentage** | [**RelatedCollectionACCSCapPercentage**](RelatedCollectionACCSCapPercentage.md) |  | [optional] 


