# AccountManagementService::RelatedCollectionTrustee

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**results** | [**Array&lt;Trustee&gt;**](Trustee.md) |  | [optional] 


