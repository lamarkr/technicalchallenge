# AccountManagementService::Address

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **String** | Type of Address | [optional] 
**street_line1** | **String** | Address line 1 | [optional] 
**street_line2** | **String** | Address line 2 | [optional] 
**suburb** | **String** | Address suburb | [optional] 
**state** | **String** | Address state | [optional] 
**postcode** | **String** | address postcode | [optional] 
**start_date** | **String** | The validity start date of this Address | [optional] 
**end_date** | **String** | The validity end date of this Address | [optional] 


