# AccountManagementService::ServiceLocationOfRecord

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**date_of_event** | **String** | Date of event for the update | [optional] 
**address** | [**RelatedCollectionAddress**](RelatedCollectionAddress.md) |  | [optional] 


