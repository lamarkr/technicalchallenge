# AccountManagementService::RelatedCollectionApprovalCondition

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**results** | [**Array&lt;ApprovalCondition&gt;**](ApprovalCondition.md) |  | [optional] 


