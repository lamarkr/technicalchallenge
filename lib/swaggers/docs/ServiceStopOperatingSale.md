# AccountManagementService::ServiceStopOperatingSale

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**new_name** | **String** | Name of the new Provider | [optional] 
**contact_name** | **String** | Name for the contact person | [optional] 
**email** | **String** | Email address | [optional] 
**phone** | **String** | Phone number | [optional] 
**mobile** | **String** | Mobile number | [optional] 
**expected_date** | **String** | Anticipated settlement date | [optional] 


