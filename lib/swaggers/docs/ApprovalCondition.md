# AccountManagementService::ApprovalCondition

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **String** | Code of Approval Condition | [optional] 
**start_date** | **String** | Start Date of Condition | [optional] 
**end_date** | **String** | End Date of Condition | [optional] 
**notification_timeframe** | **Integer** | Notification timeframe &lt;Number of days&gt; | [optional] 
**condition_reason** | **String** | Condition Reason | [optional] 
**condition_reason_text** | **String** | Condition Reason Text | [optional] 
**condition_to_apply** | **String** | Condition to apply &lt;free text&gt; | [optional] 
**place_limit_to_apply** | **Integer** | Place limit to apply &lt;Number&gt; | [optional] 
**number_of_educators_per1_coordinator** | **Integer** | Number of Educators per 1 co-ordinator &lt;Number&gt; | [optional] 
**educator_limit** | **Integer** | Educators limit &lt;Number&gt; | [optional] 
**regions** | **String** | &lt;List of regions (free text)&gt; | [optional] 
**number_of_home_visit** | **Integer** | Number of home visit &lt;Number&gt; | [optional] 
**number_or_percentage_home_visit_type** | **String** | Percentage or Actual Number | [optional] 
**number_or_percentage_home_visit** | **String** | Number/Percentage of educatory homes that require visits &lt;1-100 or 1%-100%&gt; | [optional] 
**frequency_of_visit** | **String** | Frequency of visit &lt;Frequency type&gt; | [optional] 
**personnels** | [**RelatedCollectionPersonnel**](RelatedCollectionPersonnel.md) |  | [optional] 
**records_to_be_provided_set** | [**RelatedCollectionAccountManagementRecordsToBeProvided**](RelatedCollectionAccountManagementRecordsToBeProvided.md) |  | [optional] 


