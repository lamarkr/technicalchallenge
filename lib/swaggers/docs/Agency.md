# AccountManagementService::Agency

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | Name of the agency | [optional] 


