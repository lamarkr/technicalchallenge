# AccountManagementService::RelatedCollectionServiceLocationOfRecord

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**results** | [**Array&lt;ServiceLocationOfRecord&gt;**](ServiceLocationOfRecord.md) |  | [optional] 


